#ifndef _CORE_H_
#define  _CORE_H_

// Para saber si estamos en Windows
#ifdef _WIN32
    #define IS_WIN 1
    #define CLEAR system("cls");
#else
    #define IS_WIN 0
    #define CLEAR system("clear");
#endif

// Librerias estandar
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <dirent.h>
#include <string.h>

// TDA's
#include "core/list.h"
#include "core/table.h"

// Librerias propias
#include "core/vehiculo.h"
#include "core/carManage.h"
#include "core/get.h"
#include "core/menu.h"


#endif
