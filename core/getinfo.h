#ifndef _GETINFO_H
#define  _GETINFO_H

typedef struct{
    char* nombre;
    char** tipos;
    int espacios;
    int** tarifa;
} configuracion;

// Permite obtener la informacion del archivo de configuracion o ingresar
// la configuracion inicial dependiendo del caso.
void getInfo();

#endif
