#ifndef _VEHICULO_H
#define  _VEHICULO_H

typedef struct {
    char* patente;
    int hora;
    int espacio;
} vehiculo;

// Retorna un nuevo vehiculo con su hora de ingreso y un espacio
// asignado en el estacionamiento
vehiculo* newVehiculo(char* patente);

#include "vehiculo.c"
#endif
